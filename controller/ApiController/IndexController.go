package ApiController

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"time"
)

// auth wyl
// email 1328228163
// day   10:59

type MyClaims struct {
	Uid int //自定义属性 用于不通接口传值
	jwt.StandardClaims
}

var jwtKey = []byte("abdsadaswqedq")
var expireTime = time.Now().Add(24 * time.Hour).Unix()

type IndexController struct {
}

func (i IndexController) Index(c *gin.Context) {

	//实力化 存储token的结构体
	MyClaimsObj := MyClaims{
		25,
		jwt.StandardClaims{
			ExpiresAt: expireTime,
			Issuer:    "phpwyl",
		},
	}
	//使用指定的签名方法创建签名对象
	tokenObj := jwt.NewWithClaims(jwt.SigningMethodHS256, MyClaimsObj)
	//使用指定的secret 签名兵获取完整编码后的字符串token
	tokenStr, err := tokenObj.SignedString(jwtKey)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(200, gin.H{"message": err, "success": "error"})
		return
	}
	c.JSON(200, gin.H{"message": "生成token成功", "token": tokenStr, "success": "success"})
}

func (i IndexController) CheckToken(c *gin.Context) {
	header := c.GetHeader("Authorization")
	fmt.Printf("====%v====", header)
}

func ParseToken(tokenStr string) (*jwt.Token, *MyClaims, error) {
	s := &MyClaims{}
	token, err := jwt.ParseWithClaims(tokenStr, s, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	return token, s, err
}
