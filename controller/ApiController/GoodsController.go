package ApiController

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"goxiaomi/models/es"
	"net/http"
	"strconv"
)

// auth wyl
// email 1328228163
// day   19:22

type GoodsController struct {
}

func (g GoodsController) GoodsIndexCreate(c *gin.Context) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
			c.String(http.StatusOK, "search")
		}
	}()
	//创建索引
	result := es.EsGoods{}.CreatIndex("goods", `{
	"settings":{
		"number_of_shards":1,
		"number_of_replicas":0
	},
	"mappings":{
		"doc":{
			"properties":{
				"title":{
					"type": "text",
					"analyzer": "ik_max_word",
					"search_analyzer": "ik_max_word"
				},
				"content":{
					"type":"text",
					"analyzer":"ik_max_word",
					"search_analyzer":"ik_max_word"
				}
			}
		}
	}
}
`)
	if result {
		c.JSON(200, gin.H{"status": "ok"})
		return
	}
	c.JSON(200, gin.H{"status": "error"})
}

func (g GoodsController) GoodsAdd(c *gin.Context) {
	defer func() {
		if r := recover(); r != nil {
			c.JSON(200, gin.H{"status": "no", "err": r})
		}
	}()
	goodsList := []models.Goods{}
	models.DB.Find(&goodsList)
	for i, goods := range goodsList {
		fmt.Println(goodsList[i])
		es.EsGoods{}.Add(strconv.Itoa(goods.Id), goodsList[i])
	}
	c.JSON(200, gin.H{"status": "ok"})
}
