package indexController

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"time"
)

// auth wyl
// email 1328228163
// day   22:13
type IndexController struct {
	BaseController
}

func (i IndexController) Index(c *gin.Context) {
	startTime := time.Now().UnixMilli()
	//判断redis中是否存在
	var rs models.RedisStore
	//banner
	FocusList := []models.Focus{}
	if HasFocusList := rs.Get("FocusList", &FocusList); !HasFocusList {
		models.DB.Where("status = 1 and focus_type = 1").Order("sort desc").Find(&FocusList)
		rs.Set("FocusList", &FocusList, int64(time.Second*3600))
	}

	//手机楼层
	var phoneList []models.Goods
	if HasGoodsList := rs.Get("phoneList", &phoneList); !HasGoodsList {
		phoneList = models.Goods{}.GetGoodsCate(1, "best", 8)
		rs.Set("phoneList", &phoneList, int64(time.Second*3600))
	}
	endTime := time.Now().UnixMilli()
	fmt.Printf("使用时间====%v", endTime-startTime)
	//电视 盒子
	i.Render(c, "mi/index/index.html", gin.H{
		"FocusList": FocusList,
		"phoneList": phoneList,
	})
}
func (i IndexController) Product(c *gin.Context) {
	id := c.Param("id")
	//fmt.Println(id)
	//获取所有id
	cateInfo := models.GoodsCate{}
	models.DB.Where("id = ? ", id).Find(&cateInfo)
	ids := []int{}
	if cateInfo.Pid > 0 {
		cateInfoList := []models.GoodsCate{}
		models.DB.Where("pid = ? ", cateInfo.Pid).Find(&cateInfoList)
		for _, v := range cateInfoList {
			ids = append(ids, v.Id)
		}

	}
	ids = append(ids, cateInfo.Pid)
	goodsList := []models.Goods{}
	models.DB.Where("cate_id in (?)", ids).Find(&goodsList)
	//电视 盒子
	i.Render(c, "mi/index/productList.html", gin.H{"goodsList": goodsList})
}
