package indexController

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"goxiaomi/models"
	"strings"
	"time"
)

// auth wyl
// email 1328228163
// day   21:46

type BaseController struct {
}

func (this *BaseController) Render(c *gin.Context, path string, param map[string]interface{}) {
	TopNavList := []models.Nav{}
	var rs models.RedisStore
	if HasTopNavList := rs.Get("TopNavList", &TopNavList); !HasTopNavList {
		models.DB.Where("status = 1 and  position=1").Order("sort desc").Find(&TopNavList)
		rs.Set("TopNavList", &TopNavList, int64(time.Second*3600))
	}
	MiddleNavList := []models.Nav{}
	if HasMiddleNavList := rs.Get("MiddleNavList", &MiddleNavList); !HasMiddleNavList {
		models.DB.Where("status = 1 and  position=2").Order("sort desc").Find(&MiddleNavList)
		for k, v := range MiddleNavList {
			if v.Link != "" {
				//查询商品
				GoodsList := []models.Goods{}
				ids := strings.Split(v.Relation, ",")
				models.DB.Where("id in ? and status = 1", ids).Select("id", "price", "goods_img").Order("sort desc").Find(&GoodsList)
				MiddleNavList[k].GoodsList = GoodsList
			}
		}
		rs.Set("MiddleNavList", &MiddleNavList, int64(time.Second*3600))
	}
	//GoodsCatList
	GoodsCatList := []models.GoodsCate{}
	if HasGoodsCatList := rs.Get("GoodsCatList", &GoodsCatList); !HasGoodsCatList {
		models.DB.Where("pid = 0 and status = 1").Preload("GoodsCateItems", func(db *gorm.DB) *gorm.DB {
			return db.Where("status = 1").Order("sort desc")
		}).Order("sort desc").Find(&GoodsCatList)
		rs.Set("GoodsCatList", &GoodsCatList, int64(time.Second*3600))
	}
	param["TopNavList"] = TopNavList
	param["MiddleNavList"] = MiddleNavList
	param["GoodsCatList"] = GoodsCatList
	c.HTML(200, path, param)
}
