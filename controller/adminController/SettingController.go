package adminController

import (
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"net/http"
)

type SettingController struct {
	BaseController
}

func (con SettingController) Index(c *gin.Context) {
	setting := models.Setting{Id: 1}
	models.DB.Find(&setting)
	c.HTML(http.StatusOK, "admin/setting/index.html", gin.H{
		"setting": setting,
	})
}

func (con SettingController) DoEdit(c *gin.Context) {
	setting := models.Setting{Id: 1}
	models.DB.Find(&setting)
	if err := c.ShouldBind(&setting); err != nil {
		con.error(c, "参数错误！请重新尝试", "/admin/setting")
		return
	} else {
		image, err := models.Upload{}.UploadImage(c, "site_logo", true)
		if err == nil {
			setting.SiteLogo = image
		}
		noPicture, err := models.Upload{}.UploadImage(c, "no_picture", true)
		if err == nil {
			setting.NoPicture = noPicture
		}
		err = models.DB.Save(&setting).Error
		if err != nil {
			con.error(c, "保存失败", "/admin/setting")
			return
		}
	}
	con.success(c, "保存成功", "/admin/setting")

}
