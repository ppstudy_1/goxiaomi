package adminController

import "C"
import (
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"net/http"
)

type NavController struct {
	BaseController
}

func (g NavController) Index(c *gin.Context) {
	navList := []models.Nav{}
	models.DB.Find(&navList)
	c.HTML(http.StatusOK, "admin/nav/index.html", gin.H{
		"navList": navList,
	})
}

func (g NavController) Add(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/nav/add.html", gin.H{})
}

func (g NavController) DoAdd(c *gin.Context) {
	nav := models.Nav{}
	nav.Position, _ = models.Int(c.PostForm("position"))
	nav.Title = c.PostForm("title")
	nav.AddTime = models.PhpStrtotime()
	nav.Link = c.PostForm("link")
	nav.Relation = c.PostForm("relation")
	nav.IsOpennew, _ = models.Int(c.PostForm("is_opennew"))
	nav.Status, _ = models.Int(c.PostForm("status"))
	nav.Sort, _ = models.Int(c.PostForm("sort"))
	err := models.DB.Save(&nav).Error
	if err != nil {
		g.error(c, "增加数据失败", "/admin/nav")
		return
	}
	g.success(c, "增加数据成功", "/admin/nav")
}

func (g NavController) Edit(c *gin.Context) {
	id, _ := models.Int(c.Query("id"))
	Nav := models.Nav{}
	models.DB.Where("id = ? ", id).Find(&Nav)
	c.HTML(http.StatusOK, "admin/nav/edit.html", gin.H{
		"nav": Nav,
	})
}

func (g NavController) DoEdit(c *gin.Context) {
	Nav := models.Nav{}
	id, _ := models.Int(c.PostForm("id"))
	Nav.Id = id
	Nav.Title = c.PostForm("title")
	Nav.Relation = c.PostForm("relation")
	Nav.Link = c.PostForm("link")
	Nav.IsOpennew, _ = models.Int(c.PostForm("is_opennew"))
	Nav.Status, _ = models.Int(c.PostForm("status"))
	Nav.Sort, _ = models.Int(c.PostForm("sort"))
	err := models.DB.Where("id = ?", id).Updates(Nav).Error
	if err != nil {
		g.error(c, "修改失败", "/admin/nav")
		return
	}
	g.success(c, "修改成功", "/admin/nav")
}
func (g NavController) Delete(c *gin.Context) {
	id, _ := models.Int(c.Query("id"))
	nav := models.Nav{Id: id}
	err := models.DB.Delete(&nav).Error
	if err != nil {
		g.error(c, "删除失败", "/admin/nav")
		return
	}
	g.success(c, "删除成功", "/admin/nav")

}
