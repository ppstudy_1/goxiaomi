package adminController

import "C"
import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"net/http"
)

type FocusController struct {
	BaseController
}

func (con FocusController) Index(c *gin.Context) {
	focusList := []models.Focus{}
	models.DB.Find(&focusList)
	c.HTML(http.StatusOK, "admin/focus/index.html", gin.H{"focusList": focusList})
}
func (con FocusController) Add(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/focus/add.html", gin.H{})
}

func (con FocusController) Edit(c *gin.Context) {
	id, err1 := models.Int(c.Query("id"))
	if err1 != nil {
		con.error(c, "Id错误："+err1.Error(), "/admin/focus")
		return
	}
	focusList := models.Focus{}
	models.DB.Where("id=?", id).Find(&focusList)
	c.HTML(http.StatusOK, "admin/focus/edit.html", gin.H{
		"focusList": focusList,
	})
}

func (con FocusController) Delete(c *gin.Context) {
	i, err := models.Int(c.Query("id"))
	if err != nil {
		con.error(c, "id:"+err.Error(), "/admin/focus")
	}
	focus := models.Focus{}
	err = models.DB.Where("id=?", i).Delete(&focus).Error
	if err != nil {
		con.error(c, "删除失败:"+err.Error(), "/admin/focus")
	}
	con.success(c, "删除成功", "/admin/focus/")
	c.HTML(http.StatusOK, "admin/focus/delete.html", gin.H{})
}

func (con FocusController) DoAdd(c *gin.Context) {
	title := c.PostForm("title")
	link := c.PostForm("link")
	focusType, err1 := models.Int8(c.PostForm("focus_type"))
	sort, err2 := models.Int(c.PostForm("sort"))
	status, err3 := models.Int8(c.PostForm("status"))
	if err1 != nil {
		con.error(c, "类型错误", "/admin/focus/add")
		return
	}
	if err2 != nil {
		con.error(c, "排序错误", "/admin/focus/add")
		return
	}
	if err3 != nil {
		con.error(c, "状态错误", "/admin/focus/add")
		return
	}
	//上传
	path, err4 := models.Upload{}.UploadImage(c, "focus_img", true)
	if err4 != nil {
		con.error(c, "文件上传失败", "/admin/focus/add")
		return
	}
	focus := models.Focus{
		Title:     title,
		FocusType: focusType,
		FocusImg:  path,
		Link:      link,
		Sort:      sort,
		Status:    status,
		AddTime:   int(models.UnixCurrentTime()),
	}
	err1 = models.DB.Create(&focus).Error
	if err1 != nil {
		con.error(c, err1.Error(), "/admin/focus/add")
		return
	}
	con.success(c, "添加成功", "/admin/focus/add")
}

func (con FocusController) DoEdit(c *gin.Context) {
	id, err := models.Int(c.PostForm("id"))
	title := c.PostForm("title")
	link := c.PostForm("link")
	focusType, err1 := models.Int8(c.PostForm("focus_type"))
	sort, err2 := models.Int(c.PostForm("sort"))
	status, err3 := models.Int8(c.PostForm("status"))
	if err != nil {
		con.error(c, "Id错误", "/admin/focus/edit?id="+fmt.Sprintf("%v", id))
		return
	}
	if err1 != nil {
		con.error(c, "类型错误", "/admin/focus/edit?id="+fmt.Sprintf("%v", id))
		return
	}
	if err2 != nil {
		con.error(c, "排序错误", "/admin/focus/edit?id="+fmt.Sprintf("%v", id))

		return
	}
	if err3 != nil {
		con.error(c, "状态错误", "/admin/focus/edit?id="+fmt.Sprintf("%v", id))
		return
	}
	//我们判断文件是否
	FocusImg := ""
	file, err := c.FormFile("focus_img")
	if file == nil {
		//判断
		hiddenImg := c.PostForm("hiddenImg")
		if hiddenImg != "" {
			FocusImg = hiddenImg
		}
	} else {
		image, err := models.Upload{}.UploadImage(c, "focus_img", true)
		if err == nil {
			FocusImg = image
		} else {
			con.error(c, "上传失败", "/admin/focus/edit?id="+fmt.Sprintf("%v", id))
			return
		}
	}

	//上传
	focus := models.Focus{
		Id:        id,
		Title:     title,
		FocusType: focusType,
		FocusImg:  FocusImg,
		Link:      link,
		Sort:      sort,
		Status:    status,
		AddTime:   int(models.UnixCurrentTime()),
	}
	err1 = models.DB.Where("id=?", id).Updates(&focus).Error
	if err1 != nil {
		con.error(c, err1.Error(), "/admin/focus/edit?id="+fmt.Sprintf("%v", id))
		return
	}
	con.success(c, "修改成功", "/admin/focus")
}
