package adminController

import (
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"net/http"
)

type GoodsTypeController struct {
	BaseController
}

func (g GoodsTypeController) Index(c *gin.Context) {
	goodsType := []models.GoodsType{}
	models.DB.Find(&goodsType)
	c.HTML(200, "admin/goodsType/index.html", gin.H{
		"goodsTypeList": goodsType,
	})
}

func (g GoodsTypeController) Add(c *gin.Context) {
	c.HTML(200, "admin/goodsType/add.html", gin.H{})
}

func (g GoodsTypeController) DoAdd(c *gin.Context) {
	title := c.PostForm("title")
	description := c.PostForm("description")
	status, err1 := models.Int(c.PostForm("status"))
	if err1 != nil {
		g.error(c, "非法请求", "/admin/goodsType/add")
		return
	}
	goodsType := models.GoodsType{
		Title:       title,
		Description: description,
		Status:      status,
	}
	err := models.DB.Create(&goodsType).Error
	if err != nil {
		g.error(c, "添加失败", "/admin/goodsType/add")
		return
	}
	g.success(c, "添加成功", "/admin/goodsType")
}

func (g GoodsTypeController) Edit(c *gin.Context) {
	id, err1 := models.Int(c.Query("id"))
	if err1 != nil {
		g.error(c, "非法请求", "/admin/goodsType")
		return
	}
	var goodsType models.GoodsType
	models.DB.Find(&goodsType, id)
	c.HTML(http.StatusOK, "admin/goodsType/edit.html", gin.H{
		"goodsType": goodsType,
	})
}

func (g GoodsTypeController) DoEdit(c *gin.Context) {
	id, err1 := models.Int(c.PostForm("id"))
	if err1 != nil {
		g.error(c, "非法请求", "/admin/goodsType")
		return
	}
	var goodsType models.GoodsType
	models.DB.Find(&goodsType, id)
	goodsType.Title = c.PostForm("title")
	goodsType.Description = c.PostForm("description")
	status, err2 := models.Int(c.PostForm("status"))
	if err2 != nil {
		g.error(c, "非法请求", "/admin/goodsType")
		return
	}
	goodsType.Status = status
	dbErr := models.DB.Save(goodsType).Error
	if dbErr != nil {
		g.error(c, "添加失败", "/admin/goodsType")
		return
	}
	g.success(c, "修改成功", "/admin/goodsType")

}
func (g GoodsTypeController) Delete(c *gin.Context) {
	id, idErr := models.Int(c.Query("id"))
	if idErr != nil {
		g.error(c, "非法请求", "/admin/goodsType")
		return
	}
	sqlErr := models.DB.Delete(models.GoodsType{}, id).Error
	if sqlErr != nil {
		g.error(c, "删除失败", "/admin/goodsType")
	}
	g.success(c, "删除成功", "/admin/goodsType")

}
