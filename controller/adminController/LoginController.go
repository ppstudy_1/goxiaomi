package adminController

import (
	"encoding/json"
	"fmt"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"net/http"
)

type LoginController struct {
	BaseController
}

func (con LoginController) Index(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/login/login.html", gin.H{})
}

func (con LoginController) DoLogin(c *gin.Context) {
	captchaId := c.PostForm("captchaId")
	verifyValue := c.PostForm("verify")
	password := c.PostForm("password")
	username := c.PostForm("username")
	captcha := models.VerifyCaptcha(captchaId, verifyValue)
	if captcha {
		//验证账号密码是否存在
		admin := []models.Manager{}
		models.DB.Where("username=? AND password=? AND status=1", username, models.Md5(password)).Find(&admin)
		if len(admin) > 0 {
			//session 记录
			session := sessions.Default(c)
			marshal, err := json.Marshal(admin)
			if err != nil {
				con.error(c, "服务器出错了", "/admin/login")
			}
			session.Set("userinfo2", marshal)
			session.Save()
			con.success(c, "登录成功", "/admin")
		} else {
			con.error(c, "账号或密码错误", "/admin/login")
		}
		return
	}
	con.error(c, "验证码验证 失败", "/admin/login")
}

func (con LoginController) Captcha(c *gin.Context) {
	captcha, s, err := models.MakeCaptcha()
	if err != nil {
		fmt.Println(err)
		return
	}
	c.JSON(http.StatusOK, gin.H{"CaptchaId": captcha, "captchaImage": s})
}

func (con LoginController) LoginOut(c *gin.Context) {
	session := sessions.Default(c)
	session.Delete("userinfo2")
	session.Save()
	con.success(c, "退出成功", "/admin/login")
}
