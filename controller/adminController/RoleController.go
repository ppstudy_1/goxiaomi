package adminController

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"net/http"
	"strconv"
	strings2 "strings"
)

type RoleController struct {
	BaseController
}

func (con RoleController) Index(c *gin.Context) {
	role := []models.Role{}
	models.DB.Find(&role)
	c.HTML(http.StatusOK, "admin/role/index.html", gin.H{"role": role})
}
func (con RoleController) Add(c *gin.Context) {

	c.HTML(http.StatusOK, "admin/role/add.html", gin.H{})
}

func (con RoleController) Edit(c *gin.Context) {
	id := strings2.Trim(c.Query("id"), "")
	atoi, err := strconv.Atoi(id)
	if err != nil {
		con.error(c, "参数错误", "/admin/role")
		return
	}
	role := models.Role{ID: int64(atoi)}

	err = models.DB.Find(&role).Error
	if err != nil {
		con.error(c, "失败", "/admin/role")
		return
	}

	c.HTML(http.StatusOK, "admin/role/edit.html", gin.H{"role": role})
}

func (con RoleController) Delete(c *gin.Context) {
	id := strings2.Trim(c.Query("id"), "")
	atoi, err := strconv.Atoi(id)
	if err != nil {
		con.error(c, "参数错误", "/admin/role")
		return
	}
	//先查询
	role := models.Role{ID: int64(atoi)}
	models.DB.Find(&role)
	err = models.DB.Delete(&role).Error
	if err != nil {
		con.error(c, "删除失败", "/admin/role")
		return
	}
	con.success(c, "删除成功", "/admin/role")

}

func (con RoleController) DoAdd(c *gin.Context) {
	title := strings2.Trim(c.PostForm("title"), "")
	description := strings2.Trim(c.PostForm("description"), "")
	role := models.Role{}
	role.Title = title
	role.Description = description
	role.Status = 1
	role.AddTime = models.UnixCurrentTime()
	err := models.DB.Save(&role).Error
	if err != nil {
		con.error(c, "服务器错误", "/admin/role")
		return
	}
	con.success(c, "添加角色成功", "/admin/role")
}
func (con RoleController) DoEdit(c *gin.Context) {
	id := strings2.Trim(c.PostForm("id"), "")
	title := strings2.Trim(c.PostForm("title"), "")
	description := strings2.Trim(c.PostForm("description"), "")
	atoi, err := strconv.Atoi(id)
	if err != nil {
		con.error(c, "参数错误", "/admin/role")
		return
	}
	role := models.Role{ID: int64(atoi)}

	err = models.DB.Find(&role).Error
	if err != nil {
		con.error(c, "失败", "/admin/role")
		return
	}
	role.Title = title
	role.Description = description
	err = models.DB.Save(&role).Error
	if err != nil {
		con.error(c, "服务器错误", "/admin/role")
		return
	}
	con.success(c, "添加角色成功", "/admin/role")
}

func (r RoleController) Auth(c *gin.Context) {
	id := c.Query("id")
	//所有的权限
	accessList := []models.Access{}
	models.DB.Preload("AccessItem").Where("module_id=?", 0).Find(&accessList)
	//查询拥有的权限
	rolleAccess := []models.RoleAccess{}
	models.DB.Where("role_id=?", id).Find(&rolleAccess)
	m := make(map[int]int)
	for _, v := range rolleAccess {
		m[int(v.AccessId)] = 1
	}
	for i, v := range accessList {
		if _, ok := m[int(v.Id)]; ok {
			accessList[i].Checked = true
		}
		for kk, vv := range v.AccessItem {
			if _, ok := m[int(vv.Id)]; ok {
				accessList[i].AccessItem[kk].Checked = true
			}
		}
	}

	c.HTML(200, "admin/role/auth.html", gin.H{
		"accessList": accessList,
		"roleId":     id,
	})
}
func (r RoleController) DoAuth(c *gin.Context) {
	roleId, err := models.Int(c.PostForm("role_id"))
	accessIds := c.PostFormArray("access_node[]")
	if err != nil {
		r.error(c, err.Error(), "/admin/role")
		return
	}
	//删除所有权限
	access := models.RoleAccess{}
	models.DB.Where("role_id=?", roleId).Delete(&access)
	fmt.Print("access_node : %#v", accessIds)
	//添加选中权限
	for _, v := range accessIds {
		access.AccessId, _ = models.Int64(v)
		access.RoleId = int64(roleId)
		models.DB.Create(&access)
	}
	r.success(c, "授权成功", "/admin/role/auth?id="+fmt.Sprintf("%v", roleId))
}
