package adminController

import (
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"net/http"
	"strings"
	"time"
)

type ManagerController struct {
	BaseController
}

func (con ManagerController) Index(c *gin.Context) {
	var ManagerList = []models.Manager{}
	models.DB.Preload("Role").Find(&ManagerList)
	c.HTML(http.StatusOK, "admin/manager/index.html", gin.H{
		"ManagerList": ManagerList,
	})
}
func (con ManagerController) Add(c *gin.Context) {
	//获取所有的角色
	RoleList := []models.Role{}
	models.DB.Find(&RoleList)
	c.HTML(http.StatusOK, "admin/manager/add.html", gin.H{
		"RoleList": RoleList,
	})
}
func (con ManagerController) DoAdd(c *gin.Context) {
	role_id, err1 := models.Int(c.PostForm("role_id"))
	if err1 != nil {
		con.error(c, "传入参数错误", "/admin/manager")
		return
	}
	username := strings.Trim(c.PostForm("username"), " ")
	password := strings.Trim(c.PostForm("password"), " ")
	mobile := strings.Trim(c.PostForm("mobile"), " ")
	email := strings.Trim(c.PostForm("email"), " ")
	if len(username) < 2 || len(password) < 6 {
		con.error(c, "用户名或者密码的长度不合法", "/admin/manager/add")
		return
	}
	//判断管理员是否存在
	var u = models.Manager{}
	models.DB.Where("username=?", username).First(&u)
	if u.ID > 0 {
		con.error(c, "用户已经存在，请检查", "/admin/manager/add")
		return
	}
	manager := models.Manager{
		Username: username,
		Password: models.Md5(password),
		Mobile:   mobile,
		Email:    email,
		Status:   1,
		RoleId:   int64(role_id),
		AddTime:  time.Now().Unix(),
		IsSuper:  0,
	}
	err1 = models.DB.Create(&manager).Error
	if err1 != nil {
		con.error(c, "添加失败！omg", "/admin/manager/add")
		return
	}
	con.success(c, "添加成功", "/admin/manager")
}

func (con ManagerController) Edit(c *gin.Context) {
	//查询旧的数据
	id, err1 := models.Int(c.Query("id"))
	if err1 != nil {
		con.error(c, "传入参数错误", "/admin/manager")
		return
	}
	Manager := models.Manager{ID: int64(id)}

	models.DB.Find(&Manager)
	//获取rule
	RoleList := []models.Role{}
	models.DB.Find(&RoleList)

	c.HTML(http.StatusOK, "admin/manager/edit.html", gin.H{
		"RoleList": RoleList,
		"Manager":  Manager,
	})
}
func (con ManagerController) DoEdit(c *gin.Context) {
	Password := strings.Trim(c.PostForm("password"), " ")
	Mobile := strings.Trim(c.PostForm("mobile"), " ")
	Email := strings.Trim(c.PostForm("email"), " ")
	RoleId, err1 := models.Int(c.PostForm("role_id"))
	if err1 != nil {
		con.error(c, "传入参数错误", "/admin/manager/edit")
		return
	}
	Id, idErr := models.Int(c.PostForm("id"))
	if idErr != nil {
		con.error(c, "传入参数错误", "/admin/manager/edit")
		return
	}
	var manager models.Manager
	models.DB.Where("id=?", Id).Find(&manager)
	if manager.ID < 1 {
		con.error(c, "传入参数错误", "/admin/manager/edit")
		return
	}
	manager.Mobile = Mobile
	manager.RoleId = int64(RoleId)
	manager.Email = Email
	if len(Password) > 0 {
		manager.Password = models.Md5(Password)
	}
	err1 = models.DB.Updates(&manager).Error
	if err1 != nil {
		con.error(c, "修改失败", "/admin/manager/edit")
		return
	}
	con.success(c, "修改成功", "/admin/manager")
}
func (con ManagerController) Delete(c *gin.Context) {
	Id, idErr := models.Int(c.Query("id"))
	if idErr != nil {
		con.error(c, "id传入参数错误", "/admin/manager")
		return
	}
	var manager models.Manager
	models.DB.Where("id=?", Id).Find(&manager)
	if manager.ID < 1 {
		con.error(c, "传入参数错误", "/admin/manager")
		return
	}
	idErr = models.DB.Delete(&manager).Error
	if idErr != nil {
		con.error(c, "删除错误", "/admin/manager")
		return
	}
	con.success(c, "删除成功", "/admin/manager")
}
