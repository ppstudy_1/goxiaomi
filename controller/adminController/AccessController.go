package adminController

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"net/http"
	"strconv"
	strings2 "strings"
	"time"
)

type AccessController struct {
	BaseController
}

func (con AccessController) Index(c *gin.Context) {
	Access := []models.Access{}
	models.DB.Preload("AccessItem").Where("module_id=?", 0).Find(&Access)
	c.HTML(http.StatusOK, "admin/access/index.html", gin.H{"accessList": Access})
}
func (con AccessController) Add(c *gin.Context) {
	//获取顶级id
	access := []models.Access{}
	models.DB.Where("module_id=?", 0).Find(&access)
	c.HTML(http.StatusOK, "admin/access/add.html", gin.H{
		"accessList": access,
	})
}

func (con AccessController) Edit(c *gin.Context) {
	id := strings2.Trim(c.Query("id"), "")
	atoi, err := strconv.Atoi(id)
	if err != nil {
		con.error(c, "参数错误", "/admin/Access")
		return
	}
	Access := models.Access{Id: int64(atoi)}
	accessList := []models.Access{}
	models.DB.Where("module_id=?", 0).Find(&accessList)

	err = models.DB.Find(&Access).Error
	if err != nil {
		con.error(c, "失败", "/admin/Access")
		return
	}

	c.HTML(http.StatusOK, "admin/access/edit.html", gin.H{"access": Access, "accessList": accessList})
}

func (con AccessController) Delete(c *gin.Context) {
	id := strings2.Trim(c.Query("id"), "")
	atoi, err := strconv.Atoi(id)
	if err != nil {
		con.error(c, "参数错误", "/admin/Access")
		return
	}
	//先查询
	Access := models.Access{Id: int64(atoi)}
	models.DB.Find(&Access)
	err = models.DB.Delete(&Access).Error
	if err != nil {
		con.error(c, "删除失败", "/admin/Access")
		return
	}
	con.success(c, "删除成功", "/admin/Access")

}

func (con AccessController) DoAdd(c *gin.Context) {
	ModelName := strings2.Trim(c.PostForm("module_name"), " ")
	Type, TypeErr := models.Int(c.PostForm("type"))
	ActionName := strings2.Trim(c.PostForm("action_name"), " ")
	ModuleId, ModuleIdErr := models.Int(c.PostForm("module_id"))
	Url := strings2.Trim(c.PostForm("url"), " ")
	Sort, SortErr := models.Int(c.PostForm("sort"))
	Description := strings2.Trim(c.PostForm("description"), " ")
	Status, StatusErr := models.Int(c.PostForm("status"))
	if TypeErr != nil || ModuleIdErr != nil || SortErr != nil || StatusErr != nil {
		con.error(c, "参数错误，请检查", "/admin/access/add")
		return
	}
	access := models.Access{
		ModuleName:  ModelName,
		Type:        int8(Type),
		ActionName:  ActionName,
		Url:         Url,
		ModuleId:    ModuleId,
		Sort:        Sort,
		Description: Description,
		AddTime:     time.Now().Unix(),
		Status:      Status,
	}
	SqlErr := models.DB.Create(&access).Error
	if SqlErr != nil {
		con.error(c, "添加失败，请联系客服", "/admin/access/add")
		return
	}
	con.success(c, "添加权限成功", "/admin/access")
}
func (con AccessController) DoEdit(c *gin.Context) {
	id, err1 := models.Int(c.PostForm("id"))
	moduleName := strings2.Trim(c.PostForm("module_name"), " ")
	actionName := c.PostForm("action_name")
	accessType, err2 := models.Int(c.PostForm("type"))
	url := c.PostForm("url")
	moduleId, err3 := models.Int(c.PostForm("module_id"))
	sort, err4 := models.Int(c.PostForm("sort"))
	status, err5 := models.Int(c.PostForm("status"))
	description := c.PostForm("description")
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil || err5 != nil {
		con.error(c, "传入参数错误", "/admin/access")
		return
	}
	if moduleName == "" {
		con.error(c, "模块名称不能为空", "/admin/access/edit?id="+fmt.Sprintf("%v", id))
		return
	}

	access := models.Access{Id: int64(id)}
	models.DB.Find(&access)
	access.ModuleName = moduleName
	access.Type = int8(accessType)
	access.ActionName = actionName
	access.Url = url
	access.ModuleId = moduleId
	access.Sort = sort
	access.Description = description
	access.Status = status

	err := models.DB.Save(&access).Error
	if err != nil {
		con.error(c, "修改数据", "/admin/access/edit?id="+fmt.Sprintf("%v", id))
		return
	} else {
		con.success(c, "修改数据成功", "/admin/access/edit?id="+fmt.Sprintf("%v", id))
		return
	}
	con.success(c, "修改权限成功", "/admin/access")
}
