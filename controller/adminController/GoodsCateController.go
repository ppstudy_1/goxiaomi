package adminController

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"net/http"
)

type GoodsCateController struct {
	BaseController
}

func (con GoodsCateController) Index(c *gin.Context) {
	GoodsCateList := []models.GoodsCate{}
	models.DB.Preload("GoodsCateItems").Find(&GoodsCateList)
	c.HTML(http.StatusOK, "admin/goodsCate/index.html", gin.H{"goodsCateList": GoodsCateList})
}
func (con GoodsCateController) Add(c *gin.Context) {
	cate := []models.GoodsCate{}
	models.DB.Where("pid = ?", 0).Find(&cate)
	c.HTML(http.StatusOK, "admin/goodsCate/add.html", gin.H{
		"goodsCateList": cate,
	})
}

func (con GoodsCateController) Edit(c *gin.Context) {
	id, err := models.Int(c.Query("id"))
	if err != nil {
		con.error(c, "参数错误", "/admin/goodsCate")
	}
	GoodsCateList := []models.GoodsCate{}
	models.DB.Preload("GoodsCateItems").Find(&GoodsCateList)
	goodsCate := models.GoodsCate{}
	models.DB.Find(&goodsCate, id)
	c.HTML(http.StatusOK, "admin/goodsCate/edit.html", gin.H{
		"goodsCateList": GoodsCateList,
		"goodsCate":     goodsCate,
	})
}

func (con GoodsCateController) Delete(c *gin.Context) {
	id, err := models.Int(c.Query("id"))
	if err != nil {
		con.error(c, "非法请求", "/admin/goodsCate")
		return
	}
	//判断是否有子元素
	cate := models.GoodsCate{}
	models.DB.Where("pid = ?", id).First(&cate)
	if cate.Id > 0 {
		con.error(c, "您有子元素未删除，请删除后再操作", "/admin/goodsCate")
		return
	}
	err2 := models.DB.Where("id = ?", id).Delete(&cate).Error
	if err2 != nil {
		con.error(c, "删除失败", "/admin/goodsCate")
		return
	}

	con.success(c, "删除成功", "/admin/goodsCate")
}

func (con GoodsCateController) DoAdd(c *gin.Context) {

	title := c.PostForm("title")
	if title == "" {
		con.error(c, "标题不能为空", "/admin/goodsCate/add")
		return
	}
	pid, err1 := models.Int(c.PostForm("pid"))
	link := c.PostForm("link")
	template := c.PostForm("template")
	sub_title := c.PostForm("sub_title")
	sort, err2 := models.Int(c.PostForm("sort"))
	keywords := c.PostForm("keywords")
	description := c.PostForm("description")
	status, err3 := models.Int(c.PostForm("status"))
	File, err1 := c.FormFile("cate_img")
	fmt.Printf("file:%v", File)
	image := ""
	if File != nil {
		image, err1 = models.Upload{}.UploadImage(c, "cate_img", true)
		if err1 != nil {
			con.error(c, "上传失败", "/admin/goodsCate/add")
			return
		}
	}
	fmt.Printf("%v%v%v%v%v%v%v%v%v%v", title, pid, link, template, sub_title, sort, keywords, description, status, File)
	if err1 != nil || err3 != nil {
		con.error(c, "参数错误", "/admin/goodsCate/add")
		return
	}

	if err2 != nil {
		con.error(c, "排序错误", "/admin/goodsCate/add")
		return
	}
	cate := models.GoodsCate{
		Title:       title,
		CateImg:     image,
		Link:        link,
		Template:    template,
		Pid:         pid,
		SubTitle:    sub_title,
		Keywords:    keywords,
		Description: description,
		Status:      uint8(status),
		Sort:        sort,
		AddTime:     models.UnixCurrentTime(),
	}
	sqlErr := models.DB.Create(&cate).Error
	if sqlErr != nil {
		con.error(c, "数据库插入错误", "/admin/goodsCate/add")
		return
	}
	con.success(c, "添加成功", "/admin/goodsCate/add")
}

func (con GoodsCateController) DoEdit(c *gin.Context) {
	id, err := models.Int(c.PostForm("id"))
	if err != nil {
		con.error(c, "参数错误", "/admin/goodsCate/index")
	}
	title := c.PostForm("title")
	if title == "" {
		con.error(c, "标题不能为空", "/admin/goodsCate/add")
		return
	}
	pid, err1 := models.Int(c.PostForm("pid"))
	link := c.PostForm("link")
	template := c.PostForm("template")
	sub_title := c.PostForm("sub_title")
	sort, err2 := models.Int(c.PostForm("sort"))
	keywords := c.PostForm("keywords")
	description := c.PostForm("description")
	status, err3 := models.Int(c.PostForm("status"))
	fmt.Printf("status:%v", status)

	if err1 != nil || err3 != nil {
		msg := ""
		if err1 != nil {
			msg = err1.Error()
		}
		if err3 != nil {
			msg = err3.Error()
		}
		con.error(c, msg, "/admin/goodsCate/edit?id="+fmt.Sprintf("%v", id))
		return
	}

	if err2 != nil {
		con.error(c, "排序错误", "/admin/goodsCate/edit?id="+fmt.Sprintf("%v", id))
		return
	}

	//判断id
	goodsCate := models.GoodsCate{
		Id: id,
	}
	models.DB.Find(&goodsCate)
	if goodsCate.Id < 1 {
		con.error(c, "没有该数据", "/admin/goodsCate/edit?id="+fmt.Sprintf("%v", id))
		return
	}
	goodsCate.Id = id
	goodsCate.Title = title
	goodsCate.Pid = pid
	goodsCate.Link = link
	goodsCate.Template = template
	goodsCate.SubTitle = sub_title
	goodsCate.Sort = sort
	goodsCate.Keywords = keywords
	goodsCate.Description = description
	goodsCate.Status = uint8(status)
	File, _ := c.FormFile("cate_img")
	image := ""
	if File != nil {
		image, err1 = models.Upload{}.UploadImage(c, "cate_img", true)
		if err1 != nil {
			con.error(c, "上传失败", "/admin/goodsCate/edit?id="+fmt.Sprintf("%v", id))
			return
		}
		goodsCate.CateImg = image
	}
	sqlErr := models.DB.Save(&goodsCate).Error
	if sqlErr != nil {
		con.error(c, "修改失败", "/admin/goodsCate/edit?id="+fmt.Sprintf("%v", id))
		return
	}
	con.success(c, "修改成功", "/admin/goodsCate/edit?id="+fmt.Sprintf("%v", id))
}
