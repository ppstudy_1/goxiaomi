package adminController

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"net/http"
	"time"
)

type GoodsTypeAttrController struct {
	BaseController
}

func (g GoodsTypeAttrController) Index(c *gin.Context) {
	goodsTypeId, err := models.Int(c.Query("id"))
	if err != nil {
		g.error(c, "参数错误", "/admin/goodsType")
	}
	var goodsType models.GoodsType
	models.DB.Find(&goodsType, goodsTypeId)
	var goodsTypeAttributeList []models.GoodsTypeAttribute
	models.DB.Find(&goodsTypeAttributeList, "cate_id = ?", goodsTypeId)
	c.HTML(http.StatusOK, "admin/goodsTypeAttribute/index.html", gin.H{
		"goodsType":              goodsType,
		"goodsTypeAttributeList": goodsTypeAttributeList,
		"cateId":                 goodsTypeId,
	})
}

func (g GoodsTypeAttrController) Add(c *gin.Context) {
	cateId, err := models.Int(c.Query("cate_id"))
	if err != nil {
		g.error(c, "参数错误", "/admin/goods_type")
		return
	}
	goodsTypeList := []models.GoodsType{}
	models.DB.Find(&goodsTypeList)
	c.HTML(http.StatusOK, "admin/goodsTypeAttribute/add.html", gin.H{
		"cateId":        cateId,
		"goodsTypeList": goodsTypeList,
	})

}
func (g GoodsTypeAttrController) DoAdd(c *gin.Context) {
	cate_id, err1 := models.Int(c.PostForm("cate_id"))
	if err1 != nil {
		g.error(c, "用户为空", "/admin/goodsTypeAttr/add?cate_id="+c.PostForm("cate_id"))
	}
	title := c.PostForm("title")
	if title == "" {
		g.error(c, "标题不能为空", "/admin/goodsTypeAttr/add?cate_id="+c.PostForm("cate_id"))
		return
	}

	attr_type, _ := models.Int(c.PostForm("attr_type"))
	sort, err3 := models.Int(c.PostForm("sort"))
	if err3 != nil {
		g.error(c, "排序有问题啊", "/admin/goodsTypeAttr/add?cate_id="+c.PostForm("cate_id"))
		return
	}
	attr_value := ""
	if c.PostForm("attr_value") != "" {
		attr_value = c.PostForm("attr_value")
	}
	goodsTypeAttribute := models.GoodsTypeAttribute{
		CateId:    cate_id,
		Title:     title,
		AttrType:  attr_type,
		Sort:      sort,
		AttrValue: attr_value,
		AddTime:   int(time.Now().Unix()),
	}
	dbErr := models.DB.Create(&goodsTypeAttribute).Error
	if dbErr != nil {
		g.error(c, "增加失败", "/admin/goodsTypeAttr/add?cate_id="+c.PostForm("cate_id"))
		return
	}
	g.success(c, "增加成功", "/admin/goodsTypeAttr?id="+c.PostForm("cate_id"))
	return
}
func (g GoodsTypeAttrController) Edit(c *gin.Context) {
	id, err := models.Int(c.Query("id"))
	cate_id, err1 := models.Int(c.Query("cate_id"))
	if err != nil || err1 != nil {
		g.error(c, "参数错误", "/admin/goodsTypeAttr?id="+c.Query("cate_id"))
		return
	}
	goodsTypeAttribute := models.GoodsTypeAttribute{}
	models.DB.Find(&goodsTypeAttribute, id)

	goodsTypeList := []models.GoodsType{}
	models.DB.Find(&goodsTypeList)
	c.HTML(http.StatusOK, "admin/goodsTypeAttribute/edit.html", gin.H{
		"goodsTypeAttribute": goodsTypeAttribute,
		"goodsTypeList":      goodsTypeList,
		"cate_id":            cate_id,
	})
}
func (g GoodsTypeAttrController) DoEdit(c *gin.Context) {
	id, err := models.Int(c.PostForm("id"))
	if err != nil {
		g.error(c, "参数错误", "/admin/goodsType")
		return
	}
	attribute := models.GoodsTypeAttribute{
		Id: id,
	}
	models.DB.Find(&attribute)
	if attribute.Id == 0 {
		g.error(c, "参数错误", "/admin/goodsType")
		return
	}

	title := c.PostForm("title")
	if title == "" {
		g.error(c, "标题不能为空", "/admin/goodsTypeAttr/add?cate_id="+c.PostForm("cate_id"))
		return
	}

	attr_type, _ := models.Int(c.PostForm("attr_type"))
	sort, err3 := models.Int(c.PostForm("sort"))
	if err3 != nil {
		g.error(c, "排序有问题啊", "/admin/goodsTypeAttr/add?cate_id="+c.PostForm("cate_id"))
		return
	}
	attr_value := ""
	if c.PostForm("attr_value") != "" {
		attr_value = c.PostForm("attr_value")
	}
	attribute.Title = title
	attribute.AttrType = attr_type
	attribute.AttrValue = attr_value
	attribute.Sort = sort
	err2 := models.DB.Save(&attribute).Error
	if err2 != nil {
		g.error(c, "保存失败", "/admin/goodsTypeAttr?id="+fmt.Sprintf("%v", attribute.CateId))
		return
	}
	g.success(c, "保存成功", "/admin/goodsTypeAttr?id="+fmt.Sprintf("%v", attribute.CateId))

}
func (g GoodsTypeAttrController) Delete(c *gin.Context) {
	id, err := models.Int(c.Query("id"))
	cate_id, err1 := models.Int(c.Query("cate_id"))
	if err != nil || err1 != nil {
		g.error(c, "参数错误", "/admin/goodsTypeAttr?id="+c.Query("cate_id"))
		return
	}
	attribute := models.GoodsTypeAttribute{
		Id:     id,
		CateId: cate_id,
	}
	dbErr := models.DB.Delete(&attribute).Error
	if dbErr != nil {
		g.error(c, "删除错误", "/admin/goodsTypeAttr?id="+c.Query("cate_id"))
		return
	}
	g.success(c, "删除成功", "/admin/goodsTypeAttr?id="+c.Query("cate_id"))
	return
}
