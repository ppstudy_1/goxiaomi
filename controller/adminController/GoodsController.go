package adminController

import "C"
import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"html/template"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type GoodsController struct {
	BaseController
}

func (g GoodsController) Index(c *gin.Context) {
	goodsList := []models.Goods{}
	models.DB.Where("is_delete=?", 0).Find(&goodsList)
	c.HTML(http.StatusOK, "admin/goods/index.html", gin.H{
		"goodsList": goodsList,
	})
}

func (g GoodsController) Add(c *gin.Context) {
	goodsColorList := []models.GoodsColor{}
	models.DB.Find(&goodsColorList)

	goodsCateList := []models.GoodsCate{}
	models.DB.Where("pid=0").Preload("GoodsCateItems").Find(&goodsCateList)
	goodsTypeList := []models.GoodsType{}
	models.DB.Find(&goodsTypeList)
	c.HTML(http.StatusOK, "admin/goods/add.html", gin.H{
		"goodsColorList": goodsColorList,
		"goodsCateList":  goodsCateList,
		"goodsTypeList":  goodsTypeList,
	})
}

func (g GoodsController) DoAdd(c *gin.Context) {
	title := c.PostForm("title")
	subTitle := c.PostForm("sub_title")
	cateId, _ := models.Int(c.PostForm("cate_id"))
	goodsVersion := c.PostForm("goods_version")
	price, _ := models.Float64(c.PostForm("price"))
	marketPrice, _ := models.Float64(c.PostForm("market_price"))
	status, _ := models.Int(c.PostForm("status"))
	isHot, _ := models.Int(c.PostForm("is_hot"))
	isBest, _ := models.Int(c.PostForm("is_best"))
	isNew, _ := models.Int(c.PostForm("is_new"))
	_, gimgErr := c.FormFile("goods_img")
	goodsImg := ""
	if gimgErr == nil {
		goodsImg, _ = models.Upload{}.UploadImage(c, "goods_img", true)
	}
	//content
	goodsContent := c.PostForm("goods_content")
	//商品属性
	goodsColorStr := c.PostForm("goods_color")
	relationGoods := c.PostForm("relation_goods")
	goodsGift := c.PostForm("goods_gift")
	goodsFitting := c.PostForm("goods_fitting")
	goodsAttr := c.PostForm("goods_attr")
	goodsKeywords := c.PostForm("goods_keywords")
	goodsDesc := c.PostForm("goods_desc")

	//规格与包装
	goodsTypeId, _ := models.Int(c.PostForm("goods_type_id"))
	attrIdList := c.PostFormArray("attr_id_list")
	attrValueList := c.PostFormArray("attr_value_list")

	goods := models.Goods{
		Title:         title,
		SubTitle:      subTitle,
		CateId:        cateId,
		ClickCount:    100,
		MarketPrice:   float64(marketPrice),
		Price:         float64(price),
		RelationGoods: relationGoods,
		GoodsAttr:     goodsAttr,
		GoodsVersion:  goodsVersion,
		GoodsGift:     goodsGift,
		GoodsFitting:  goodsFitting,
		GoodsKeywords: goodsKeywords,
		GoodsDesc:     goodsDesc,
		GoodsContent:  goodsContent,
		IsHot:         isHot,
		IsBest:        isBest,
		IsNew:         isNew,
		GoodsTypeId:   goodsTypeId,
		Status:        status,
		AddTime:       int(time.Now().Unix()),
		GoodsColor:    goodsColorStr,
		GoodsImg:      goodsImg,
	}

	err := models.DB.Create(&goods).Error
	if err != nil {
		g.error(c, "增加失败", "/admin/goods/add")
		return
	}

	//5、增加图库 信息

	goodsImageList := c.PostFormArray("goods_image_list")

	goodsColorist := c.PostFormArray("goods_color_list")
	for k, v := range goodsImageList {
		goodsImgObj := models.GoodsImage{}
		goodsImgObj.GoodsId = goods.Id
		goodsImgObj.ImgUrl = v
		i, _ := models.Int(goodsColorist[k])
		goodsImgObj.ColorId = i
		goodsImgObj.Sort = 10
		goodsImgObj.Status = 1
		goodsImgObj.AddTime = int(time.Now().Unix())
		models.DB.Create(&goodsImgObj)
	}

	//6、增加规格包装
	attrIdList = c.PostFormArray("attr_id_list")
	attrValueList = c.PostFormArray("attr_value_list")
	for i := 0; i < len(attrIdList); i++ {
		goodsTypeAttributeId, attributeIdErr := models.Int(attrIdList[i])
		if attributeIdErr == nil {
			//获取商品类型属性的数据
			goodsTypeAttributeObj := models.GoodsTypeAttribute{
				Id: goodsTypeAttributeId,
			}
			models.DB.Find(&goodsTypeAttributeObj)
			//给商品属性里面增加数据  规格包装
			goodsAttrObj := models.GoodsAttr{}
			goodsAttrObj.GoodsId = goods.Id
			goodsAttrObj.AttributeTitle = goodsTypeAttributeObj.Title
			goodsAttrObj.AttributeType = goodsTypeAttributeObj.AttrType
			goodsAttrObj.AttributeId = goodsTypeAttributeObj.Id
			goodsAttrObj.AttributeCateId = goodsTypeAttributeObj.CateId
			goodsAttrObj.AttributeValue = attrValueList[i]
			goodsAttrObj.Status = 1
			goodsAttrObj.Sort = 10
			goodsAttrObj.AddTime = int(time.Now().Unix())
			models.DB.Create(&goodsAttrObj)
		}

	}

	g.success(c, "增加数据成功", "/admin/goods")
}

func (g GoodsController) Edit(c *gin.Context) {
	id, err := models.Int(c.Query("id"))
	if err != nil {
		g.error(c, "id错误！", "/admin/goods")
	}
	goods := models.Goods{}
	models.DB.Find(&goods, id)

	goodsColorList := []models.GoodsColor{}
	models.DB.Find(&goodsColorList)

	goodsColorSplit := strings.Split(goods.GoodsColor, ",")
	colorMap := make(map[int]string)
	for _, v := range goodsColorSplit {
		i, _ := models.Int(v)
		colorMap[i] = ""
	}

	for key, v := range goodsColorList {
		if _, ok := colorMap[v.Id]; ok {
			goodsColorList[key].Checked = true
		} else {
			goodsColorList[key].Checked = false
		}
	}
	//商品所属分类
	goodsCateList := []models.GoodsCate{}
	models.DB.Where("pid=0").Preload("GoodsCateItems").Find(&goodsCateList)
	for k, v := range goodsCateList {
		if v.Id == goods.CateId {
			goodsCateList[k].Checked = true
		}
		for kk, vv := range goodsCateList[k].GoodsCateItems {
			if vv.Id == goods.CateId {
				goodsCateList[k].GoodsCateItems[kk].Checked = true
			}
		}
	}

	//商品所属的类型
	goodsTypeList := []models.GoodsType{}
	models.DB.Preload("GoodsTypeAttribute").Find(&goodsTypeList)
	attrHtml := ""
	for k, goodsType := range goodsTypeList {
		if goods.GoodsTypeId == goodsType.Id {
			goodsTypeList[k].Checked = true
			for _, vv := range goodsType.GoodsTypeAttribute {
				//查询
				attr := models.GoodsAttr{}
				models.DB.Where("goods_id=? and attribute_cate_id=? and  attribute_id=?", goods.Id, goods.GoodsTypeId, vv.Id).Find(&attr)
				fmt.Printf("===%v===attr", attr)
				if vv.AttrType == 1 {
					attrHtml += fmt.Sprintf(`<li><span>%s: 　</span> <input type="hidden" name="attr_id_list" value="%s" />   <input type="text" name="attr_value_list" value="%s"  /></li>`, vv.Title, strconv.Itoa(vv.Id), attr.AttributeValue)
				} else if vv.AttrType == 2 {
					attrHtml += fmt.Sprintf(`<li><span>%s: 　</span> <input type="hidden" name="attr_id_list" value="%s">  <textarea cols="50" rows="3" name="attr_value_list" value="%s">%s</textarea></li>`, vv.Title, strconv.Itoa(vv.Id), attr.AttributeValue, attr.AttributeValue)
				} else if vv.AttrType == 3 {
					attrHtml += fmt.Sprintf(`<li><span>%s: 　</span>  <input type="hidden" name="attr_id_list" value="%s" />`, vv.Title, strconv.Itoa(vv.Id))
					attrHtml += `<select name="attr_value_list">`
					split := strings.Split(vv.AttrValue, `
`)
					fmt.Printf("%v===vv.AttrValue", vv.AttrValue)
					for kk, vv := range split {
						if vv == attr.AttributeValue {
							attrHtml += fmt.Sprintf(`<option value="%s" selected>%s</option>`, strconv.Itoa(kk), vv)
						} else {
							attrHtml += fmt.Sprintf(`<option value="%s">%s</option>`, strconv.Itoa(kk), vv)
						}
					}

					attrHtml += `</select>`
					attrHtml += `</li>`
				}
			}
		}

	}
	//图库
	goodsImageList := []models.GoodsImage{}
	models.DB.Where("goods_id=?", goods.Id).Find(&goodsImageList)
	fmt.Printf("colorList:%v", goodsColorList)
	c.HTML(http.StatusOK, "admin/goods/edit.html", gin.H{
		"goodsList":      goods,
		"goodsColorList": goodsColorList,
		"goodsCateList":  goodsCateList,
		"goodsTypeList":  goodsTypeList,
		"goodsImageList": goodsImageList,
		"attrHtml":       template.HTML(attrHtml),
		"id":             goods.Id,
	})
}

func (g GoodsController) DoEdit(c *gin.Context) {
	id, err := models.Int(c.PostForm("id"))
	if err != nil {
		g.error(c, "错误:id"+err.Error(), "/admin/goods")
		return
	}

	title := c.PostForm("title")
	subTitle := c.PostForm("sub_title")
	cateId, _ := models.Int(c.PostForm("cate_id"))
	goodsVersion := c.PostForm("goods_version")
	price, _ := models.Float64(c.PostForm("price"))
	marketPrice, _ := models.Float64(c.PostForm("market_price"))
	status, _ := models.Int(c.PostForm("status"))
	isHot, _ := models.Int(c.PostForm("is_hot"))
	isBest, _ := models.Int(c.PostForm("is_best"))
	isNew, _ := models.Int(c.PostForm("is_new"))
	_, gimgErr := c.FormFile("goods_img")
	goodsImg := ""
	if gimgErr == nil {
		goodsImg, _ = models.Upload{}.UploadImage(c, "goods_img", true)
	}
	//content
	goodsContent := c.PostForm("goods_content")
	//商品属性
	goodsColorList := c.PostFormArray("goods_color")
	goodsColorStr := strings.Join(goodsColorList, ",")

	relationGoods := c.PostForm("relation_goods")
	goodsGift := c.PostForm("goods_gift")
	goodsFitting := c.PostForm("goods_fitting")
	goodsAttr := c.PostForm("goods_attr")
	goodsKeywords := c.PostForm("goods_keywords")
	goodsDesc := c.PostForm("goods_desc")

	//规格与包装
	goodsTypeId, _ := models.Int(c.PostForm("goods_type_id"))

	goods := models.Goods{
		Title:         title,
		SubTitle:      subTitle,
		CateId:        cateId,
		ClickCount:    100,
		MarketPrice:   float64(marketPrice),
		Price:         float64(price),
		RelationGoods: relationGoods,
		GoodsAttr:     goodsAttr,
		GoodsVersion:  goodsVersion,
		GoodsGift:     goodsGift,
		GoodsFitting:  goodsFitting,
		GoodsKeywords: goodsKeywords,
		GoodsDesc:     goodsDesc,
		GoodsContent:  goodsContent,
		IsHot:         isHot,
		IsBest:        isBest,
		IsNew:         isNew,
		GoodsTypeId:   goodsTypeId,
		Status:        status,
		AddTime:       int(time.Now().Unix()),
		GoodsColor:    goodsColorStr,
	}
	if goodsImg != "" {
		goods.GoodsImg = goodsImg
	}
	err1 := models.DB.Where("id=?", id).Updates(&goods).Error
	if err1 != nil {
		g.error(c, "修改失败", "/admin/goods/edit?id="+strconv.Itoa(id))
		return
	}
	attrIdList := c.PostFormArray("attr_id_list")
	attrValueList := c.PostFormArray("attr_value_list")

	goodsImageList := c.PostFormArray("goods_image_list")
	goodsImgColorList := c.PostFormArray("goods_color_list")
	//删除所有的图片
	goodsImgObj := models.GoodsImage{}
	models.DB.Where("goods_id =?", id).Delete(&goodsImgObj)
	for k, v := range goodsImageList {
		goodsImgObj = models.GoodsImage{}
		goodsImgObj.GoodsId = id
		goodsImgObj.ImgUrl = v
		goodsImgObj.Sort = 10
		colorId, _ := models.Int(goodsImgColorList[k])
		goodsImgObj.ColorId = colorId
		goodsImgObj.Status = 1
		goodsImgObj.AddTime = int(time.Now().Unix())
		models.DB.Create(&goodsImgObj)
	}

	//删除属性
	attr := models.GoodsAttr{}
	models.DB.Where("goods_id = ?", id).Delete(&attr)
	for i := 0; i < len(attrIdList); i++ {
		goodsTypeAttributeId, attributeIdErr := models.Int(attrIdList[i])
		if attributeIdErr == nil {
			//获取商品类型属性的数据
			goodsTypeAttributeObj := models.GoodsTypeAttribute{
				Id: goodsTypeAttributeId,
			}
			models.DB.Find(&goodsTypeAttributeObj)
			//给商品属性里面增加数据  规格包装
			goodsAttrObj := models.GoodsAttr{}
			goodsAttrObj.GoodsId = id
			goodsAttrObj.AttributeTitle = goodsTypeAttributeObj.Title
			goodsAttrObj.AttributeType = goodsTypeAttributeObj.AttrType
			goodsAttrObj.AttributeId = goodsTypeAttributeObj.Id
			goodsAttrObj.AttributeCateId = goodsTypeAttributeObj.CateId
			goodsAttrObj.AttributeValue = attrValueList[i]
			goodsAttrObj.Status = 1
			goodsAttrObj.Sort = 10
			goodsAttrObj.AddTime = int(time.Now().Unix())
			models.DB.Create(&goodsAttrObj)
		}

	}

	g.success(c, "修改成功", "/admin/goods/edit?id="+strconv.Itoa(id))
}
func (g GoodsController) Delete(c *gin.Context) {
	id, err := models.Int(c.Query("id"))
	if err != nil {
		g.error(c, "错误", "/admin/goods/")
		return
	}
	goods := models.Goods{}
	models.DB.Where("id=?", id).Find(&goods)
	if goods.Id < 1 {
		g.error(c, "ID错误", "/admin/goods/")
		return
	}
	goods.IsDelete = 1
	err = models.DB.Updates(&goods).Error
	if err != nil {
		g.error(c, "删除失败", "/admin/goods/")
		return
	}
	g.success(c, "删除成功", "/admin/goods/")

}

func (g GoodsController) GoodsTypeAttribute(c *gin.Context) {
	cateId, _ := models.Int(c.Query("cateId"))

	//查询id
	attribute := []models.GoodsTypeAttribute{}
	models.DB.Where("cate_id=?", cateId).Find(&attribute)
	c.JSON(http.StatusOK, gin.H{
		"success": 1,
		"result":  attribute,
	})

}
