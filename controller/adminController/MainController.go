package adminController

import (
	"encoding/json"
	"fmt"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"goxiaomi/models"
	"net/http"
)

type MainController struct {
	BaseController
}

func (m MainController) Index(c *gin.Context) {
	session := sessions.Default(c)
	userinfo := session.Get("userinfo2")
	userinfoStr, ok := userinfo.([]byte)
	userName := ""
	accessList := []models.Access{}
	if ok {
		var manager []models.Manager
		json.Unmarshal(userinfoStr, &manager)
		userName = manager[0].Username
		//所有菜单都要查到

		models.DB.Preload("AccessItem", func(db *gorm.DB) *gorm.DB {
			return db.Order("sort desc")
		}).Where("module_id=?", 0).Order("sort desc").Find(&accessList)
		//获取我的角色权限
		roleAccess := []models.RoleAccess{}
		models.DB.Where("role_id=?", manager[0].RoleId).Find(&roleAccess)
		//将我的权限id放到一个map中
		accessIdMap := make(map[int]int)
		for _, v := range roleAccess {
			accessIdMap[int(v.AccessId)] = 1
		}
		//我们需要对所有对accessList 判断是否拥有其权限
		for k, v := range accessList {
			if manager[0].IsSuper == 1 {
				accessList[k].Checked = true
			} else if _, ok := accessIdMap[int(v.Id)]; ok {
				accessList[k].Checked = true
			}
			for kk, vv := range v.AccessItem {
				if vv.Type == 2 {
					if manager[0].IsSuper == 1 {
						accessList[k].AccessItem[kk].Checked = true
					} else if _, ok := accessIdMap[int(v.Id)]; ok {
						accessList[k].AccessItem[kk].Checked = true
					}
				}
			}
		}
	}
	fmt.Printf("%#v", accessList)
	c.HTML(http.StatusOK, "admin/main/index.html", gin.H{
		"username":   userName,
		"accessList": accessList,
	})
}
func (m MainController) Welcome(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/main/welcome.html", gin.H{})
}
func (m MainController) FullCache(c *gin.Context) {
	err := models.RedisDb.FlushDB(models.RedisDb.Context()).Err()
	if err != nil {
		m.error(c, "清除失败", "/admin/welcome")
		return
	}
	m.success(c, "清除成功", "/admin/welcome")
}

func (m MainController) StatusUpdate(c *gin.Context) {
	id := c.PostForm("id")
	table := c.PostForm("table")
	status := c.PostForm("status")
	field := c.PostForm("field")
	err := models.DB.Exec("update " + table + " set " + field + " = Abs(" + status + "-1) where id = " + id).Error
	if err != nil {
		c.JSON(200, gin.H{
			"error": 1,
			"msg":   "修改失败",
		})
		return
	}
	c.JSON(200, gin.H{
		"error": 0,
		"msg":   "修改成功",
	})
}

func (m MainController) NumberUpdate(c *gin.Context) {
	id := c.PostForm("id")
	table := c.PostForm("table")
	num := c.PostForm("num")
	field := c.PostForm("field")
	err := models.DB.Exec("update " + table + " set " + field + " = " + num + " where id = " + id).Error
	if err != nil {
		c.JSON(200, gin.H{
			"error": 1,
			"msg":   "修改失败",
		})
		return
	}
	c.JSON(200, gin.H{
		"error": 0,
		"msg":   "修改成功",
	})
}

func (m MainController) UploadImg(c *gin.Context) {
	_, err := c.FormFile("custom-field-name")
	if err != nil {
		c.JSON(200, gin.H{
			"error":   1,
			"message": err.Error(),
		})
		return
	}
	image, err := models.Upload{}.UploadImage(c, "custom-field-name", true)
	if err != nil {
		c.JSON(200, gin.H{
			"error":   1,
			"message": err.Error(),
		})
		return
	}
	data := gin.H{
		"errno": 0,
		"data": gin.H{
			"url": image,
		},
	}
	c.JSON(200, data)
}

func (m MainController) WebUploadImg(c *gin.Context) {
	_, err := c.FormFile("file")
	if err != nil {
		c.JSON(200, gin.H{
			"error":   1,
			"message": err.Error(),
		})
		return
	}
	image, err := models.Upload{}.UploadImage(c, "file", true)
	if err != nil {
		c.JSON(200, gin.H{
			"error":   1,
			"message": err.Error(),
		})
		return
	}
	data := gin.H{
		"errno": 0,
		"data": gin.H{
			"url": image,
		},
	}
	c.JSON(200, data)
}
