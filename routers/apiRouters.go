package routers

import (
	"github.com/gin-gonic/gin"
	"goxiaomi/controller/ApiController"
)

// auth wyl
// email 1328228163
// day   10:55
func ApiRoutersInit(r *gin.Engine) {
	r.GET("/api/v1/test", ApiController.IndexController{}.Index)
	r.GET("/api/v1/check_token", ApiController.IndexController{}.CheckToken)

	//创建索引
	r.GET("/api/v1/goods_index_create", ApiController.GoodsController{}.GoodsIndexCreate)
	//增加数据
	r.GET("/api/v1/goods_add", ApiController.GoodsController{}.GoodsAdd)

}
