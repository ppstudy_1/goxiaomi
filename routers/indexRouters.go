package routers

import (
	"github.com/gin-gonic/gin"
	"goxiaomi/controller/indexController"
)

func IndexRoutersInit(r *gin.Engine) {
	indexGroup := r.Group("/")
	{
		indexGroup.GET("/", indexController.IndexController{}.Index)
		indexGroup.GET("/index", indexController.IndexController{}.Index)
		indexGroup.GET("/product/:id", indexController.IndexController{}.Product)
	}
}
