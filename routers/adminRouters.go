package routers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goxiaomi/controller/adminController"
	"goxiaomi/middleware"
)

func LoginMiddleware(c *gin.Context) {
	fmt.Println("后台中间件1")
	c.Set("name", "name123")
	c.Next()
	fmt.Println("后台中间件2")
}
func AdminRoutersInit(r *gin.Engine) {
	adminGroup := r.Group("/admin", middleware.InitAdminAuthMiddleware)
	{
		adminGroup.GET("/", adminController.MainController{}.Index)
		adminGroup.GET("/welcome", adminController.MainController{}.Welcome)
		adminGroup.GET("/fullcahe", adminController.MainController{}.FullCache)
		adminGroup.POST("/statusUpdate", adminController.MainController{}.StatusUpdate)
		adminGroup.POST("/numberUpdate", adminController.MainController{}.NumberUpdate)
		adminGroup.POST("/upload-image", adminController.MainController{}.UploadImg)
		adminGroup.POST("/web-upload-img", adminController.MainController{}.WebUploadImg)

		//adminGroup.Use(LoginMiddleware)
		adminGroup.GET("/login", adminController.LoginController{}.Index)
		adminGroup.POST("/doLogin", adminController.LoginController{}.DoLogin)
		adminGroup.GET("/captcha", adminController.LoginController{}.Captcha)
		adminGroup.GET("/loginOut", adminController.LoginController{}.LoginOut)

		adminGroup.GET("/manager", adminController.ManagerController{}.Index)
		adminGroup.GET("/manager/add", adminController.ManagerController{}.Add)
		adminGroup.POST("/manager/doAdd", adminController.ManagerController{}.DoAdd)
		adminGroup.POST("/manager/doEdit", adminController.ManagerController{}.DoEdit)
		adminGroup.GET("/manager/edit", adminController.ManagerController{}.Edit)
		adminGroup.GET("/manager/delete", adminController.ManagerController{}.Delete)

		//role 角色路由
		adminGroup.GET("/role", adminController.RoleController{}.Index)
		adminGroup.GET("/role/add", adminController.RoleController{}.Add)
		adminGroup.GET("/role/edit", adminController.RoleController{}.Edit)
		adminGroup.GET("/role/delete", adminController.RoleController{}.Delete)
		adminGroup.POST("/role/doAdd", adminController.RoleController{}.DoAdd)
		adminGroup.POST("/role/doEdit", adminController.RoleController{}.DoEdit)
		adminGroup.GET("/role/auth", adminController.RoleController{}.Auth)
		adminGroup.POST("/role/doAuth", adminController.RoleController{}.DoAuth)

		//access
		adminGroup.GET("/access", adminController.AccessController{}.Index)
		adminGroup.GET("/access/add", adminController.AccessController{}.Add)
		adminGroup.GET("/access/edit", adminController.AccessController{}.Edit)
		adminGroup.GET("/access/delete", adminController.AccessController{}.Delete)
		adminGroup.POST("/access/doAdd", adminController.AccessController{}.DoAdd)
		adminGroup.POST("/access/doEdit", adminController.AccessController{}.DoEdit)
		//banner
		adminGroup.GET("/focus", adminController.FocusController{}.Index)
		adminGroup.GET("/focus/add", adminController.FocusController{}.Add)
		adminGroup.GET("/focus/edit", adminController.FocusController{}.Edit)
		adminGroup.GET("/focus/delete", adminController.FocusController{}.Delete)
		adminGroup.POST("/focus/doAdd", adminController.FocusController{}.DoAdd)
		adminGroup.POST("/focus/doEdit", adminController.FocusController{}.DoEdit)

		//商品
		adminGroup.GET("/goods", adminController.GoodsController{}.Index)
		adminGroup.GET("/goods/add", adminController.GoodsController{}.Add)
		adminGroup.GET("/goods/edit", adminController.GoodsController{}.Edit)
		adminGroup.GET("/goods/delete", adminController.GoodsController{}.Delete)
		adminGroup.POST("/goods/doAdd", adminController.GoodsController{}.DoAdd)
		adminGroup.POST("/goods/doEdit", adminController.GoodsController{}.DoEdit)
		adminGroup.GET("/goods/goodsTypeAttribute", adminController.GoodsController{}.GoodsTypeAttribute)
		//goodCate
		adminGroup.GET("/goodsCate", adminController.GoodsCateController{}.Index)
		adminGroup.GET("/goodsCate/add", adminController.GoodsCateController{}.Add)
		adminGroup.GET("/goodsCate/edit", adminController.GoodsCateController{}.Edit)
		adminGroup.GET("/goodsCate/delete", adminController.GoodsCateController{}.Delete)
		adminGroup.POST("/goodsCate/doAdd", adminController.GoodsCateController{}.DoAdd)
		adminGroup.POST("/goodsCate/doEdit", adminController.GoodsCateController{}.DoEdit)

		//商品类型
		adminGroup.GET("/goodsType", adminController.GoodsTypeController{}.Index)
		adminGroup.GET("/goodsType/add", adminController.GoodsTypeController{}.Add)
		adminGroup.GET("/goodsType/edit", adminController.GoodsTypeController{}.Edit)
		adminGroup.GET("/goodsType/delete", adminController.GoodsTypeController{}.Delete)
		adminGroup.POST("/goodsType/doAdd", adminController.GoodsTypeController{}.DoAdd)
		adminGroup.POST("/goodsType/doEdit", adminController.GoodsTypeController{}.DoEdit)
		//商品类型属性
		adminGroup.GET("/goodsTypeAttr", adminController.GoodsTypeAttrController{}.Index)
		adminGroup.GET("/goodsTypeAttr/add", adminController.GoodsTypeAttrController{}.Add)
		adminGroup.GET("/goodsTypeAttr/edit", adminController.GoodsTypeAttrController{}.Edit)
		adminGroup.GET("/goodsTypeAttr/delete", adminController.GoodsTypeAttrController{}.Delete)
		adminGroup.POST("/goodsTypeAttr/doAdd", adminController.GoodsTypeAttrController{}.DoAdd)
		adminGroup.POST("/goodsTypeAttr/doEdit", adminController.GoodsTypeAttrController{}.DoEdit)

		//导航管理

		adminGroup.GET("/nav", adminController.NavController{}.Index)
		adminGroup.GET("/nav/add", adminController.NavController{}.Add)
		adminGroup.GET("/nav/edit", adminController.NavController{}.Edit)
		adminGroup.GET("/nav/delete", adminController.NavController{}.Delete)
		adminGroup.POST("/nav/doAdd", adminController.NavController{}.DoAdd)
		adminGroup.POST("/nav/doEdit", adminController.NavController{}.DoEdit)

		//商店设置
		adminGroup.GET("/setting", adminController.SettingController{}.Index)
		adminGroup.POST("/setting/doEdit", adminController.SettingController{}.DoEdit)

	}
}
