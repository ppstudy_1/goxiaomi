package main

import (
	"github.com/gin-contrib/sessions"
	cookie2 "github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"goxiaomi/models"
	"goxiaomi/routers"
	"html/template"
)

type Student struct {
	Name string `json:"name,omitempty"`
	Age  int    `json:"age,omitempty"`
}

func main() {
	r := gin.Default() //创建默认对路由引擎
	r.SetFuncMap(template.FuncMap{
		"UnixToTime": models.UnixToTime,
		"toInt":      models.IntNoErr,
		"countList":  models.CountList,
		"MbSubstr":   models.MbSubstr,
	})
	r.LoadHTMLGlob("view/**/**/*")
	r.Static("/static", "./static")
	////创建基于cookie的存储引擎，secret11111 参数适用于加密的密钥
	cookie := cookie2.NewStore([]byte("112233"))
	//配置session的中间件，store是前面创建的存储引擎，我们可以替换成其他存储引擎
	r.Use(sessions.Sessions("mysession", cookie))
	routers.AdminRoutersInit(r)
	routers.IndexRoutersInit(r)
	routers.ApiRoutersInit(r)
	r.Run("127.0.0.1:8001")
}
