$(function(){
	
	$('.aside h4').click(function(){
		
//		$(this).toggleClass('active');
		
		
		$(this).siblings('ul').slideToggle();
	})
	$(".statusUpdate").click(function (){
		//获取id
		var id = $(this).attr('data-id');
		//获取表名称
		var table = $(this).attr('data-table');
		//获取状态
		var status =  $(this).attr('data-status');
		//获取字段
		var field = $(this).attr('data-field');
		obj=$(this)
		//ajax请求
		$.ajax({
			url: "/admin/statusUpdate", // 要请求的远程服务器地址
			type: "POST", // 请求类型，可以是GET、POST等
			dataType: "json", // 预期服务器返回的数据类型
			data: {
				id:id,
				table:table,
				status:status,
				field:field
			}, // 发送到服务器的数据，可以是一个对象或字符串
			success: function(response) {
				// 请求成功时执行的回调函数，参数为服务器返回的数据
				if (response.error == 0){
					var src =obj.children().attr("src")
					if (src.indexOf("no") != -1 ){
						obj.attr("data-status",1)
						src = "/static/admin/images/yes.gif"
					}else{
						obj.attr("data-status",0)
						src = "/static/admin/images/no.gif"
					}

					obj.children().prop("src",src)
				}else if (response.err ==1){
					alert("修改失败")
				}
			},
			error: function(xhr, status, error) {
				// 请求失败时执行的回调函数，参数为XMLHttpRequest对象、状态码和错误信息
				alert("请求失败")
				console.error("请求失败:", status, error);
			}
		});

	})
	$(".numUpdate").click(function (){
		$(this).on("click", function(event) {
			event.stopPropagation();
			console.log("Child clicked");
		});
		var id = $(this).attr('data-id');
		//获取表名称
		var table = $(this).attr('data-table');
		//获取状态
		var num =  $(this).attr('data-num');
		//获取字段
		var field = $(this).attr('data-field');
		var spanEl = $(this)

		var input = $("<input style='width:60px'  value='' />");
		$(this).html(input)
		$(input).trigger("focus").val(num);
		$(input).click(function (e) {
			e.stopPropagation();
		})
		$(input).blur(function () {
			var inputNum = $(this).val()
			spanEl.html(inputNum)
			//触发ajax请求
			$.post("/admin/numberUpdate", { id: id, table: table, field: field, num: inputNum }, function (response) {

				if(response.error != 0){
					alert("修改失败")
					spanEl.html(num)
				}else{
					spanEl.attr("data-num",inputNum)
				}
			})
		})

	})
})
