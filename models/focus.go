package models

type Focus struct {
	Id        int    `json:"id"`
	Title     string `json:"title"`
	FocusType int8   `json:"focus_type"`
	FocusImg  string `json:"focus_img"`
	Link      string `json:"link"`
	Sort      int    `json:"sort"`
	Status    int8   `json:"status"`
	AddTime   int    `json:"add_time"`
}

// TableName 表名称
func (*Focus) TableName() string {
	return "focus"
}
