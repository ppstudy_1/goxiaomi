package models

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-ini/ini"
	"os"
	"path/filepath"
	"strings"
)

type Upload struct {
}

// UploadImage 用于上传图片文件并返回文件路径
// 参数:
// - c: gin的上下文对象，用于处理HTTP请求和响应
// - formData: 表单字段名称，用于获取上传的文件
// - IsMd5: 校验文件MD5值，比对成功将引用素材库中的路径（需要考虑的是，如果后期做路由删除问题，删除一张导致多个位置失效）
// 返回值:
// - string: 上传成功后的文件路径
// - error: 错误对象，如果有错误发生，将返回该错误
func (up Upload) UploadImage(c *gin.Context, formData string, IsMd5 bool) (string, error) {
	File, err1 := c.FormFile(formData)
	if err1 != nil {
		return "", err1
	}
	//我们判断格式
	m := map[string]bool{
		".jpg":  true,
		".png":  true,
		".jpeg": true,
	}
	//c.SaveUploadedFile()
	ext := filepath.Ext(File.Filename)
	if _, ok := m[ext]; !ok {
		return "", errors.New("您输入的图片类型不支持")
	}
	//判断文件大小
	if File.Size > 1024*1024*5 {
		return "", errors.New("您输入的图片过大，不能超过5M")
	}
	savePath := ""
	//判断 IsMd5
	if IsMd5 == true {
		md5, err1 := GetFileMd5(File)
		if err1 != nil {
			return "", errors.New("无法获取文件的Md5值")
		}
		//如果 md5 存在 查询 素材表中是否有该文件
		sourceData := Source{}
		DB.Where("file_md5=? and type=?", md5, 1).Find(&sourceData)
		if sourceData.Id > 0 { //存在
			//判断文件是否存在？
			stat, err1 := os.Stat(sourceData.Url)
			if err1 == nil {
				return strings.Trim(sourceData.Url, "."), nil
			} else {
				fmt.Printf("文件不存在%v", stat)
				savePath = GetSaveMd5Path(md5 + ext)
				uploadErr := c.SaveUploadedFile(File, savePath)
				if uploadErr != nil {
					return "", uploadErr
				}
				return strings.Trim(savePath, "."), nil
			}
		} else { //不存在id
			//进行上传操作
			savePath = GetSaveMd5Path(md5 + ext)
			uploadErr := c.SaveUploadedFile(File, savePath)
			if uploadErr != nil {
				return "", uploadErr
			}
			//组合数据增加到数据库中
			sourceData.Url = strings.Trim(savePath, ".")
			sourceData.Type = 1
			sourceData.FileMd5 = md5
			sourceData.Ext = ext
			sourceData.FileSize = int(File.Size)
			sourceData.FileName = File.Filename
			uploadErr = DB.Create(&sourceData).Error
			if uploadErr != nil {
				return "", uploadErr
			}
			return strings.Trim(savePath, "."), nil
		}
	} else {
		savePath = GetSavePath(PhpDate("YmdHis") + "_" + generateRandomNumber(10) + ext)
		uploadErr := c.SaveUploadedFile(File, savePath)
		if uploadErr != nil {
			return "", uploadErr
		}
		return strings.Trim(savePath, "."), nil
	}
}

func GetSaveMd5Path(fileName string) string {
	load, loadErr := ini.Load("./ini/config.ini")
	if loadErr != nil {
		os.Exit(0)
	}
	UploadMd5Path := load.Section("").Key("UploadMd5Path").String()
	return UploadMd5Path + "/" + PhpDate("Y_m_d") + "/" + fileName
}

func GetSavePath(fileName string) string {
	load, loadErr := ini.Load("./ini/config.ini")
	if loadErr != nil {
		os.Exit(0)
	}
	UploadPath := load.Section("").Key("UploadPath").String()
	return UploadPath + "/general/" + PhpDate("Y_m_d") + "/" + fileName
}
