package models

type Access struct {
	Id          int64    `json:"id"`
	ModuleName  string   `json:"module_name"`
	Type        int8     `json:"type"`
	ActionName  string   `json:"action_name"`
	Url         string   `json:"url"`
	ModuleId    int      `json:"module_id"`
	Sort        int      `json:"sort"`
	Description string   `json:"description"`
	AddTime     int64    `json:"add_time"`
	Status      int      `json:"status"`
	AccessItem  []Access `gorm:"foreignKey:ModuleId;references:Id"`
	Checked     bool     `gorm:"-"`
}

// TableName 表名称
func (*Access) TableName() string {
	return "access"
}
