package models

// Manager undefined
type Manager struct {
	ID       int64  `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Mobile   string `json:"mobile"`
	Email    string `json:"email"`
	Status   int8   `json:"status"`
	RoleId   int64  `json:"role_id"`
	AddTime  int64  `json:"add_time"`
	IsSuper  int8   `json:"is_super"`
	Role     Role   `gorm:"foreignKey:RoleId"`
}

// TableName 表名称
func (*Manager) TableName() string {
	return "manager"
}
