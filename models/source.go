package models

import (
	"gorm.io/gorm"
	"time"
)

type Source struct {
	Id          int       `db:"id"`
	AdminName   string    `db:"admin_name"`
	Url         string    `db:"url"`
	FileSize    int       `db:"file_size"`
	FileName    string    `db:"file_name"`
	Type        int8      `db:"type"`
	FileMd5     string    `db:"file_md5"`
	Ext         string    `db:"ext"`
	CreatedTime time.Time `gorm:"default:current_timestamp" db:"created_time"`
	UpdatedTime time.Time `gorm:"default:current_timestamp" db:"updated_time"`
}

func (*Source) TableName() string {
	return "source"
}

func (s *Source) BeforeCreate(tx *gorm.DB) (err error) {
	s.CreatedTime = time.Now()
	s.UpdatedTime = time.Now()
	return
}

func (s *Source) BeforeUpdate(tx *gorm.DB) (err error) {
	s.UpdatedTime = time.Now()
	return
}
