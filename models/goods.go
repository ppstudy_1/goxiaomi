package models

import "fmt"

type Goods struct {
	Id            int
	Title         string
	SubTitle      string
	GoodsSn       string
	CateId        int
	ClickCount    int
	GoodsNumber   int
	Price         float64
	MarketPrice   float64
	RelationGoods string
	GoodsAttr     string
	GoodsVersion  string
	GoodsImg      string
	GoodsGift     string
	GoodsFitting  string
	GoodsColor    string
	GoodsKeywords string
	GoodsDesc     string
	GoodsContent  string
	IsDelete      int
	IsHot         int
	IsBest        int
	IsNew         int
	GoodsTypeId   int
	Sort          int
	Status        int
	AddTime       int
}

func (Goods) TableName() string {
	return "goods"
}

/**
    * 获取商品分类
	* @param int $cateId 分类ID
	* @param string $goodsType 商品类型
	* @param int $limitNum 获取数量
*/
func (g Goods) GetGoodsCate(cateId int, goodsType string, limitNum int) []Goods {
	cate := GoodsCate{}
	DB.Find(&cate, cateId)
	goodsList := []Goods{}
	if cate.Id < 0 {
		return goodsList
	}
	ids := []int{}
	ids = append(ids, cateId)
	if cate.Pid == 0 { //1级分类 ，需要获取下面的子集分类
		cates := []GoodsCate{}
		DB.Find(&cates, "pid = ?", cate.Id)
		for _, v := range cates {
			ids = append(ids, v.Id)
		}
	}
	where := "1=1"
	switch goodsType {
	case "hot":
		where += " AND is_hot=1"
	case "best":
		where += " AND is_best=1"
	case "new":
		where += " AND is_new=1"
	default:
		break

	}
	fmt.Printf("ids %v", ids)
	DB.Where("cate_id in ?", ids).Where(where).Limit(limitNum).Order("sort desc").Find(&goodsList)
	return goodsList
}
