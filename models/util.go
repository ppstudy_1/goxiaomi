package models

import (
	"crypto/md5"
	"fmt"
	"io"
	"math/rand"
	"mime/multipart"
	"reflect"
	"strconv"
	"time"
	"unicode/utf8"
)

func Md5(string2 string) string {
	h := md5.New()
	io.WriteString(h, string2)
	return fmt.Sprintf("%x", h.Sum(nil))
}

// 判断字符串是否存在于切片中的函数
func StringInSlice(searchString string, mySlice []string) bool {
	for _, value := range mySlice {
		if value == searchString {
			return true
		}
	}
	return false
}

func Int(str string) (int, error) {
	i, err := strconv.Atoi(str)
	return i, err
}
func Float64(str string) (float64, error) {
	i, err := strconv.ParseFloat(str, 64)
	return i, err
}
func IntNoErr(str string) int {
	i, _ := strconv.Atoi(str)
	return i
}
func CountList(a interface{}, add int) int {
	value := reflect.ValueOf(a)
	switch value.Kind() {
	case reflect.Array, reflect.Slice, reflect.Map, reflect.Chan, reflect.String:
		return value.Len() + add
	default:
		return 0
	}
}
func MbSubstr(str string, start, end int) string {
	// 将字符串转换为rune数组，支持Unicode字符
	runes := []rune(str)

	// 计算字符串的长度
	strLen := utf8.RuneCountInString(str)

	// 处理负数索引和超出字符串长度索引情况
	if start < 0 {
		start = strLen + start
	}
	if start < 0 {
		start = 0
	}
	if start >= strLen {
		return ""
	}

	end = start + end

	if end > strLen {
		end = strLen
	}

	return string(runes[start:end])
}
func Int64(str string) (int64, error) {
	i, err := strconv.Atoi(str)
	return int64(i), err
}

func Int8(str string) (int8, error) {
	intValue, err := strconv.Atoi(str)
	if err != nil {
		return 0, err
	}

	intValue8 := int8(intValue)
	return intValue8, nil
}

// 获取文件的md5值
func GetFileMd5(file *multipart.FileHeader) (string, error) {
	open, err2 := file.Open()
	if err2 != nil {
		return "", err2
	}
	defer open.Close()
	hash := md5.New()
	if _, err := io.Copy(hash, open); err != nil {
		return "", err
	}
	md5Hash := fmt.Sprintf("%x", hash.Sum(nil))
	return md5Hash, nil
}

// 生成随机数
func generateRandomNumber(length int) string {
	const charset = "0123456789"
	// 设置种子以确保每次运行都产生不同的随机数
	rand.Seed(time.Now().UnixNano())
	// 生成随机数
	randomNumber := make([]byte, length)
	for i := 0; i < length; i++ {
		randomNumber[i] = charset[rand.Intn(len(charset))]
	}
	return string(randomNumber)
}
