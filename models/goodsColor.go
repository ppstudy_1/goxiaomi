package models

// auth wyl
// email 1328228163
// day   16:30
import "time"

type GoodsColor struct {
	Id         int       `gorm:"column:id;primary_key"`
	ColorName  string    `gorm:"column:color_name"`
	ColorValue string    `gorm:"column:color_value"`
	Status     int       `gorm:"column:status"`
	CreatedAt  time.Time `gorm:"column:created_at"`
	UpdatedAt  time.Time `gorm:"column:updated_at"`
	Checked    bool
}

func (GoodsColor) TableName() string {
	return "goods_color"
}
