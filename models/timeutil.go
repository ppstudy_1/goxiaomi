package models

import (
	"fmt"
	"strings"
	"time"
)

// go中实现PHP的日期写发
func PhpDate(str string) string {
	str = strings.Replace(str, "Y", "2006", -1)
	str = strings.Replace(str, "y", "06", -1)
	str = strings.Replace(str, "m", "01", -1)
	str = strings.Replace(str, "d", "02", -1)
	str = strings.Replace(str, "h", "15", -1)
	str = strings.Replace(str, "i", "04", -1)
	str = strings.Replace(str, "s", "05", -1)
	return time.Now().Format(str)
}

// 获得当前时间 年月日时分秒
func GetDateTime() string {
	currentTime := time.Now()
	// 格式化时间为 "2018-02-03 11:11:11"
	return currentTime.Format("2006-01-02 15:04:05")
}

// 获得当前时间 年月日
func GetDate() string {
	currentTime := time.Now()
	// 格式化时间为 "2018-02-03 11:11:11"
	return currentTime.Format("2006-01-02")
}

func GetTime(splitter string) string {
	currentTime := time.Now()
	// 获取年月日
	year := currentTime.Year()
	month := int(currentTime.Month())
	day := currentTime.Day()
	sprintf := fmt.Sprintf("%v%v%v%v%v", year, splitter, month, splitter, day)
	return sprintf
}

// unix转日期
func UnixToTime(timestamp interface{}) string {

	t := time.Time{}

	switch v := timestamp.(type) {
	case int:
		t = time.Unix(int64(timestamp.(int)), 0)
	case int64:
		t = time.Unix(timestamp.(int64), 0)
	default:
		fmt.Printf("%v", v)
		return ""
	}

	return t.Format("2006-01-02 15:04:05")
}

// 获取unix当前时间戳
func UnixCurrentTime() int64 {
	currentTime := time.Now()
	// 获取当前时间的时间戳（Unix 时间戳，单位为秒）
	timestamp := currentTime.Unix()
	return timestamp
}

func PhpStrtotime() int {
	currentTime := time.Now()
	// 获取当前时间的时间戳（Unix 时间戳，单位为秒）
	timestamp := currentTime.Unix()
	return int(timestamp)
}
