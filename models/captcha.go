package models

import (
	"github.com/mojocn/base64Captcha"
	"image/color"
)

// 创建store
//var store = base64Captcha.DefaultMemStore

var store base64Captcha.Store = VerifyRedis{}

// 获取验证码
func MakeCaptcha() (string, string, error) {
	var driver base64Captcha.Driver
	//参数参考 captcha.mojotv.cn
	driverString := base64Captcha.DriverString{
		Height:          40,
		Width:           100,
		NoiseCount:      0,
		ShowLineOptions: 2 | 4,
		Length:          1,
		Source:          "1234567890",
		BgColor: &color.RGBA{
			R: 3,
			G: 102,
			B: 214,
			A: 125,
		},
		Fonts: []string{"wqy-microhei.ttc"},
	}
	driver = driverString.ConvertFonts()
	//fmt.Println("store:%v", store)
	c := base64Captcha.NewCaptcha(driver, store)
	id, b64s, _, err := c.Generate()
	return id, b64s, err
}

// 验证验证码
func VerifyCaptcha(id string, VerifyVale string) bool {
	if store.Verify(id, VerifyVale, true) {
		return true
	}
	return false
}
