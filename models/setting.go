package models

// auth wyl
// email 1328228163
// day   15:55

type Setting struct {
	Id              int    `json:"id" form:"id"`
	SiteTitle       string `json:"site_title" form:"site_title"`
	SiteLogo        string `json:"site_logo" `
	SiteKeywords    string `json:"site_keywords" form:"site_keywords"`
	SiteDescription string `json:"sit_description" form:"sit_description"`
	NoPicture       string `json:"no_picture"`
	SiteIcp         string `json:"site_icp" form:"site_icp"`
	SiteTel         string `json:"site_tel" form:"site_tel"`
	SearchKeywords  string `json:"search_keywords" form:"search_keywords"`
	TongjiCode      string `json:"tongji_code" form:"tongji_code"`
	Appid           string `json:"appid" form:"appid"`
	AppSecret       string `json:"app_secret"  form:"app_secret"`
	EndPoint        string `json:"end_point" form:"end_point"`
	BucketName      string `json:"bucket_name" form:"bucket_name"`
	OssStatus       int    `json:"oss_status" form:"oss_status"`
}

func (*Setting) TableName() string {
	return "setting"
}
