package models

type GoodsCate struct {
	Id             int         `gorm:"primaryKey" json:"id,omitempty"`
	Title          string      `json:"title,omitempty"`
	CateImg        string      `json:"cate_img,omitempty"`
	Link           string      `json:"link,omitempty"`
	Template       string      `json:"template,omitempty"`
	Pid            int         `json:"pid,omitempty"`
	SubTitle       string      `json:"sub_title,omitempty"`
	Keywords       string      `json:"keywords,omitempty"`
	Description    string      `json:"description,omitempty"`
	Status         uint8       `json:"status,omitempty"`
	Sort           int         `json:"sort,omitempty"`
	AddTime        int64       `json:"add_time,omitempty"`
	GoodsCateItems []GoodsCate `gorm:"foreignKey:Pid;relation:belongsTo"json:"goods_cate_items,omitempty"`
	Checked        bool        `json:"checked" gorm:"-"`
}

// TableName 表名称
func (*GoodsCate) TableName() string {
	return "goods_cate"
}
