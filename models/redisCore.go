package models

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-ini/ini"
	"github.com/go-redis/redis/v8"
	"os"
	"time"
)

var redisCorectx = context.Background()

var (
	RedisDb *redis.Client
)

type RedisStore struct {
}

func init() {
	//获取配置中的参数
	cfg, err := ini.Load("./ini/config.ini")
	if err != nil {
		os.Exit(1)
	}
	Host := cfg.Section("Redis").Key("Host").String()
	if Host == "" {
		Host = "127.0.0.1"
	}
	Port := cfg.Section("Redis").Key("Port").String()
	if Port == "" {
		Port = "6379"
	}
	Password := cfg.Section("Redis").Key("Password").String()
	Host = Host + ":" + Port
	RedisDb = redis.NewClient(&redis.Options{
		Addr:     Host,
		Password: Password,
		DB:       0,
	})

	_, err = RedisDb.Ping(redisCorectx).Result()
	if err != nil {
		fmt.Println("Redis连接出错了；%s,%s,%s", Host, Port, Password)
		os.Exit(1)
	}
}

func (rs RedisStore) Get(key string, obj interface{}) bool {
	result, err2 := RedisDb.Get(redisCorectx, key).Result()
	if err2 == nil && result != "" {
		err2 := json.Unmarshal([]byte(result), &obj)
		if err2 == nil {
			return true
		}
	}
	return false
}

func (rs RedisStore) Set(key string, value interface{}, expiration int64) bool {
	result, err := json.Marshal(value)
	if err != nil {
		return false
	}
	err = RedisDb.Set(redisCorectx, key, string(result), time.Duration(expiration)).Err()
	if err != nil {
		return false
	}
	return true
}
