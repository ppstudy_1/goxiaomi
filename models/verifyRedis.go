package models

import (
	"fmt"
	"time"
)

const CAPTCHA = "captcha:"

// auth wyl
// email 1328228163
// day   21:33

type VerifyRedis struct {
}

func (r VerifyRedis) Set(id string, value string) error {
	key := CAPTCHA + id
	err := RedisDb.Set(redisCorectx, key, value, time.Minute*2).Err()
	return err
}

func (r VerifyRedis) Get(id string, clear bool) string {
	key := CAPTCHA + id
	val, err := RedisDb.Get(redisCorectx, key).Result()
	if err != nil {
		fmt.Println(err)
		return ""
	}
	if clear {
		err := RedisDb.Del(redisCorectx, key).Err()
		if err != nil {
			fmt.Println(err)
			return ""
		}
	}
	return val
}

func (r VerifyRedis) Verify(id string, answer string, clear bool) bool {
	v := VerifyRedis{}.Get(id, clear)
	return v == answer
}
