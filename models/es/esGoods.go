package es

import (
	"context"
	"fmt"
	"github.com/olivere/elastic/v7"
	"goxiaomi/models"
)

// auth wyl
// email 1328228163
// day   20:19

type EsGoods struct {
}

func (es EsGoods) CreatIndex(index string, mapping string) bool {
	exists, err := models.EsClient.IndexExists("goods").Do(context.Background())
	if err != nil {
		// Handle error
		panic(err)
	}
	fmt.Printf("走到这里了 ，=======test ======%v\n", "aaaa")
	if !exists {
		// Create a new index.
		createIndex, err := models.EsClient.CreateIndex("goods").Body(mapping).IncludeTypeName(true).Do(context.Background())
		if err != nil {
			// Handle error
			panic(err)
		}
		if !createIndex.Acknowledged {
			// Not acknowledged
			panic("未成功生成索引")
		}
		return true
	}
	return true
}

func (es EsGoods) Add(id string, BodyJson interface{}) *elastic.IndexResponse {
	do, err := models.EsClient.Index().
		Index("goods").Type("_doc").
		Id(id).
		BodyJson(BodyJson).Do(context.Background())
	if err != nil {
		panic(err)
	}
	return do
}
