package models

// Role undefined
type Role struct {
	ID          int64  `json:"Id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Status      int8   `json:"status"`
	AddTime     int64  `json:"add_time"`
}

// TableName 表名称
func (*Role) TableName() string {
	return "role"
}
