package models

// Role undefined
type RoleAccess struct {
	RoleId   int64 `json:"role_id"`
	AccessId int64 `json:"access_id"`
}

// TableName 表名称
func (*RoleAccess) TableName() string {
	return "role_access"
}
