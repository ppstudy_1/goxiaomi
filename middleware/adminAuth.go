package middleware

import (
	"encoding/json"
	"fmt"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/go-ini/ini"
	"goxiaomi/models"
	strings2 "strings"
)

func InitAdminAuthMiddleware(c *gin.Context) {
	pathname := strings2.Split(c.Request.URL.String(), "?")[0]
	fmt.Println(pathname, "pathname")
	strings := []string{
		"/admin/login",
		"/admin/doLogin",
		"/admin/captcha",
		"/admin/loginOut",
	}

	slice := models.StringInSlice(pathname, strings)
	if slice == false {
		session := sessions.Default(c)
		userinfo := session.Get("userinfo2")

		if userinfo == nil {
			c.Redirect(302, "/admin/login")
			return
		}
		var u []models.Manager
		sprintf := fmt.Sprintf("%s", userinfo)
		err := json.Unmarshal([]byte(sprintf), &u)
		if err != nil {
			c.Redirect(302, "/admin/login")
		}
		if !(len(u) > 0 && u[0].Username != "") {
			c.Redirect(302, "/admin/login")
		}

		//我们进行权限验证
		if u[0].IsSuper != 1 && !checkedConfigPath(pathname) {
			//获取路由 /admin/manager
			//获取管理员拥有角色的权限
			roleAccessList := []models.RoleAccess{}
			models.DB.Where("role_id=?", u[0].RoleId).Find(&roleAccessList)
			userRoleMap := make(map[int64]int)
			for _, v := range roleAccessList {
				userRoleMap[v.AccessId] = 1
			}
			//通过访问的连接查询对应的access_id
			var pathAccess models.Access
			replace := strings2.Replace(pathname, "/admin", "", 1)
			models.DB.Where("url like ?", "%"+replace+"%").Find(&pathAccess)
			if _, ok := userRoleMap[pathAccess.Id]; !ok {
				c.String(200, "没有权限")
				c.Abort()
			}

		}

	}

}

// 过滤不校验的路径
func checkedConfigPath(pathName string) bool {
	cfg, err := ini.Load("./ini/config.ini")
	if err != nil {
		fmt.Println("无法加载配置文件:", err)
		return false
	}
	section := cfg.Section("").Key("AdminPath").String()
	split := strings2.Split(section, ",")
	fmt.Printf("%s,%v", section, split)
	for _, v := range split {
		if v == pathName {
			return true
		}
	}
	return false
}
